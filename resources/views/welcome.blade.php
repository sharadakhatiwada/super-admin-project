    <html>
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
    <head> 
                @if (Route::has('login'))
                    <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                        @auth
                            <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                        @else
                            <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                            @endif
                        @endif
                    </div>
                @endif

                
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Euro Cup</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="styles.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    
    <script>
        function getTimeRemaining(timerSection, endTime) {
            var t = new Date(endTime).getTime() - new Date().getTime();
            var seconds = Math.floor( (t/1000) % 60 );
            var minutes = Math.floor( (t/1000/60) % 60);
            var hours = Math.floor( (t/(1000*60*60)) % 24);
            var days = Math.floor( t/(1000*60*60*24) );
            var remainingTime = "Times Up!"
            if(t > 0){
                remainingTime = days + " Days, " + hours + ' Hours, '+ minutes + ' minutes, ' + seconds + ' seconds'; 
            }
            document.getElementById(timerSection).innerHTML = remainingTime;
        }

        function startTimer(timerSection, endTime){
            setInterval(function() {getTimeRemaining(timerSection, endTime)}, 1000);
        }
    </script>

  
<style type="text/css">
#myCarousel img {
    height:80%;
    width:100%;
}

</style>


    </head>

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        
        <div class="carousel-inner">
        
        @foreach($sliders as $key => $slider)
        <div class="carousel-item {{$key == 0 ? 'active' : '' }}">

           
                <img src="{{ URL::to('/') }}/images/{{$slider->image_name}}" alt="">
            </div>
        @endforeach                           
                                  
        </div>
        
            <a class="carousel-control-prev" href="#myCarousel" role="button"  data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true">     </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
    
    </div>





    <div class="card text-center">
    <div class="card-header">
    Game
    </div>
    @foreach($matches as $i=>$match)
    <div class="card-body">
    <div class="container">
    <div class="row">
    <div class="col-sm-4 pull-left" >
    <h4>{{$match->country1}} </h4>
    </div>
    <div class="col-sm-4 Center" >
    <h3 id="{{$i}}_timer"><script type='text/javascript'>startTimer("{{$i}}_timer", "{{$match->date_time}}");</script></h3>

    </div>
    <div class="col-sm-4 pull-right" >
    <h4>{{$match->country2}} </h4>

    </div>


    </div>

        
    </div>
    </div>
    @endforeach












    </html>