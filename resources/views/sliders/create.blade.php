@extends('home')
@section('homecontent')
<div class="row"> 
    <div class="col-sm-8 offset-sm-2">    
        <h3 class="display-4">Add Slider</h3>  
        <div>    
            @if ($errors->any())      
            <div class="alert alert-danger">        
                <ul>            
                @foreach ($errors->all() as $error)              
                <li>{{ $error }}</li>            
                @endforeach        
                </ul>      
            </div>
            <br />    
            @endif      
            <form enctype="multipart/form-data" method="post" action="{{ route('slider.store') }}">          
            @csrf 
            
            
            <div class="form-group d-flex flex-column">                  
                <label for="image">Image:</label>          
                <input type="file"  name="image"/>  
                       
            </div> 
                           
            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('slider.index') }}" class="btn btn-primary">Cancel</a>  
                
            </form>  
        </div>
    </div>
</div>
@endsection