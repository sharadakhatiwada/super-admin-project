
@extends('home')
@section('homecontent')
<div class="row"> 
    <div class="col-sm-8 offset-sm-2">    
        <h3 class="display-4">Add </h3>  
        <div>    
            @if ($errors->any())      
            <div class="alert alert-danger">        
                <ul>            
                @foreach ($errors->all() as $error)              
                <li>{{ $error }}</li>            
                @endforeach        
                </ul>      
            </div>
            <br />    
            @endif  

            <form enctype="multipart/form-data" method="post" action="{{ route('superAdmin.store') }}">          
            @csrf 
          
                
            
             
            <div class="form-group d-flex flex-column">                  
                <label for="playerName">Player Name:</label>          
                <input type="text"  name="playerName"/>  
                       
            </div> 
            
            
           
           
            <div class="form-group">
                <label for="countryName">Select Country</label>
                <select id="country-select" class="selectpicker countrypicker" name='countryName'data-flag="true" ></select>
               
                
  
            </div>
                  
                           
            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('superAdmin.index') }}" class="btn btn-primary">Cancel</a>  
                
            </form>  
        </div>
    </div>
</div>
@endsection









