

@extends('home')
@section('homecontent')

<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Super Admin</h1>   
        <a href="{{ route('superAdmin.create') }}" class="btn btn-primary">add</a>  
        <table class="table table-striped">    
            <thead>        
            <tr>          
                
                <td>Player Name</td>
                <td>Country Name</td>
               
                
               

                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($superAdmins as $superAdmin)        
                    <tr>            
                       
                        <td>{{$superAdmin->playerName}}</td>
                        <td>{{$superAdmin->countryName}}
                        <!-- <?php 
                            // echo locale_get_display_region('$superAdmin', 'en');
                            ?> -->
                        </td>
                        
                    
              
                        
                        
                        <td>                                      
                            <a href="{{ route('superAdmin.edit',$superAdmin->id)}}" class="btn btn-primary">Edit </a>
                                                                  

                        </td> 
                        
                        
                        
                        <td>
                          <form action="{{ route('superAdmin.destroy',$superAdmin->id)}}" method="post">                  
                                @csrf                  
                               @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>    
                                      
                        </td> 
                        
          
                        
                    </tr>  

                @endforeach    
            </tbody>  
        </table>
       
    <div>
    
</div>


@endsection

