@extends('home')
@section('homecontent')
<div class="row">    
    <div class="col-sm-8 offset-sm-2">        
        <h3 class="display-4">Update </h3>        
        @if ($errors->any())        
        <div class="alert alert-danger">            
            <ul>                
            @foreach ($errors->all() as $error)                
            <li>{{ $error }}</li>                
            @endforeach            
            </ul>        
        </div>        
        <br />         
        @endif        
        <form enctype="multipart/form-data" method="post" action="{{ route('superAdmin.update',$superAdmin->id) }}">            
        @method('PATCH')             
        @csrf  

            
            <div class="form-group d-flex flex-column">                  
                <label for="playerName">Player Name:</label>          
                <input type="text"  name="playerName" value='{{$superAdmin->playerName}}'/>  
                       
            </div> 
           
            
            <div class="form-group">
                <label for="countryName">Select Country</label>
                <select id="country-select" class="selectpicker countrypicker" name='countryName' data-flag="true" ></select>
                <script>
              
                $(document).ready(function() {
                    $('#country-select').val('{{$superAdmin->countryName}}');
                });

                        </script>
               
            </div>
           
                  
           

            <button type="submit" class="btn btn-success">Update</button>        
          <a href="{{ route('superAdmin.index') }}" class="btn btn-primary">Cancel</a>                        
        </form>
    </div>
</div>
@endsection