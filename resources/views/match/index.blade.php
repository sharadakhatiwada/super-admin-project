

@extends('home')
@section('homecontent')
<div class="container">
<div class="row">
<h2> Game Time</h2>
    <div class="col-md-12">    
          
        <a href="{{ route('match.create') }}" class="btn btn-primary">add</a>  
        <table class="table table-striped">    
            <thead>        
            <tr> 
                 
                
                <td>Country A</td>
                <td>VS</td>
                <td>Country B</td>

                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($match as $match)        
                    <tr>            
                   
                        <td>{{$match->country1}}</td>
                      


                      <td>{{$match->date_time}}</td> 

                      
                        <td>{{$match->country2}}</td>
                    
                        <td>                                      
                            <a href="{{ route('match.edit',$match->id)}}" class="btn btn-primary">Edit </a>
                                                                  

                        </td> 
                       
                        <td>
                          <form action="{{ route('match.destroy',$match->id)}}" method="post">                  
                                @csrf                  
                               @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>    
                                      
                        </td> 
                        
          
                        
                    </tr>  

                @endforeach    
            </tbody>  
        </table>
       
    <div>
    
</div>
</div>


@endsection

