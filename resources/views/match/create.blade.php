
@extends('home')
@section('homecontent')

    
<div class="row"> 
<thead>  

            </thead> 
            
    <div class="col-sm-8 offset-sm-2">    
        <h3 class="display-4">Add </h3>  
        <div>    
            @if ($errors->any())      
            <div class="alert alert-danger">        
                <ul>            
                @foreach ($errors->all() as $error)              
                <li>{{ $error }}</li>            
                @endforeach        
                </ul>      
            </div>
            <br />    
            @endif  

            <form enctype="multipart/form-data" method="post" action="{{ route('match.store') }}">          
            @csrf 
          
              
            <div class="form-group">
            <label for="country1">Country A:</label>          
                <input type="text"  name="country1" /> 
                </div>
            
               
                
            <div class="form-group">                   
            <label for="date_time">Game Time:</label>
                <input type="datetime-local" name="date_time">         
            </div>   
  
            </div>
            <div class="form-group">
            <label for="country2">Country B:</label>          
                <input type="text"  name="country2" /> 
                </div>
               
               

           

            
            
           
           
            
                           
            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('match.index') }}" class="btn btn-primary">Cancel</a>  
                
            </form>  
        </div>
    </div>
</div>
@endsection









