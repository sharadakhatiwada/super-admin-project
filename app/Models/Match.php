<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use HasFactory;
    public $table = 'match_scores';
    protected $fillable = [
        'country1',
        'country2',
        'date_time'

       
    ];
}
