<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Match;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $match = Match::all();
             
        return view('match.index', compact('match'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $match=Match::all();
        return view('match.create', compact('match'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        

        $request->validate([
           
            'country1',
            'country2',
            'date_time'
        ]);
        $match=new Match([
            'country1'=>$request->get('country1'),
            'country2'=>$request->get('country2'),
            'date_time'=>$request->get('date_time')

        ]);
        $match->save();
        return redirect()->route('match.index')->with('success','match saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $match=Match::find($id);
        return view('match.edit', compact('match'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       
        $request->validate([
           
            'country1',
            'country2',
            'date_time'
        ]);



        $match=Match::find($id);
        $match->country1=$request->get('country1');
        $match->country2=$request->get('countryName');
        $match->date_time=$request->get('date_time');

        $match->save();
        return redirect()->route('match.index')->with('success', 'match saved');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $match =Match::find($id);
        $match ->delete($id);
        return redirect()->route('match.index')->with('success','matchDeleted');
    }
}
