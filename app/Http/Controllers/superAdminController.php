<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SuperAdmin;

class superAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $superAdmins = SuperAdmin::all();
             
        return view('superAdmin.index', compact('superAdmins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $superAdmins=SuperAdmin::all();
        return view('superAdmin.create', compact('superAdmins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if ($request->user()->cannot('create', SuperAdmin::class)) {
            abort(403);
        }

        $request->validate([
            'playerName',
            'countryName'
        ]);
        $superAdmin=new SuperAdmin([
            'playerName'=>$request->get('playerName'),
            'countryName'=>$request->get('countryName')

        ]);
        $superAdmin->save();
        return redirect()->route('superAdmin.index')->with('success','SuperAdmin saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $superAdmin=SuperAdmin::find($id);
        return view('SuperAdmin.edit', compact('superAdmin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->user()->cannot('update', SuperAdmin::class)) {
            abort(403);
        }

        $request->validate([
            'playerName',
            'countryName'
        ]);




        $superAdmin=SuperAdmin::find($id);
        $superAdmin->playerName=$request->get('playerName');
        $superAdmin->countryName=$request->get('countryName');
        $superAdmin->save();
        return redirect()->route('superAdmin.index')->with('success', 'Super Admin saved');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy($id)
    {
        //
        $superAdmin =SuperAdmin::find($id);
        $superAdmin ->delete($id);
        return redirect()->route('superAdmin.index')->with('success','Super Admin Deleted');
    }
}
