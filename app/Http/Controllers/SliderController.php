<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Storage\Facades;
use App\Models\SliderModel;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sliders = SliderModel::all();
             
        return view('sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $sliders=SliderModel::all();
        return view('sliders.create', compact('sliders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $request->validate([            
                                
           
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                  
        ]);  
        
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name= time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $image_name);  

        }




        $slider = new SliderModel([            
                                
            
            'image_name'=>$image_name,
            'image_url'=>$destinationPath 
                         
            ]);        
        $slider->save(); 
        

        return redirect()->route('slider.index')->with('success','slider saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider=SliderModel::find($id);
        return view('sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
                   
           
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'                     
              
        ]);      
        
        $slider=SliderModel::find($id);
        File::delete(public_path("/images/").$slider->image_name);  
 
    
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $image_name);


        $slider= SliderModel::find($id);        
                  
       
        $slider->image_name=$image_name;
               $slider->image_url=$destinationPath;
              
        $slider->save();        
        return redirect()->route('slider.index')->with('success', 'Slider saved');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $slider =SliderModel::find($id);
        
        File::delete(public_path("/images/").$slider->image_name);

        
       
        $slider->delete();
        return redirect()->route('slider.index')->with('success','Slider Deleted');
    }
}
