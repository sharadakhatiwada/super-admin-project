<?php

namespace App\Console\Commands;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class RegisterSuperAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:super-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register super admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $user;
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user =$user;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    { 
        $details = $this->getDetails();
        $admin=$this->user->createSuperAdmin($details);
        $this->display($admin);
        
    }
    private function getDetails() : array{
        $details['name']=$this->ask('Name');
        $details['email']=$this->ask('Email');
        $details['password']=$this->secret('password');
        $details['confirm_password']=$this->secret('Confirm password');
        while(! $this->isValidPassword($details['password'],$details['confirm_password'])){
            if(! $this->isRequiredLength($details['password'])){
                $this->error('Password must be more than six characters');
            }
            if(! $this->isMatch($details['password'],$details['confirm_password'])){
                $this->error('password and confirm password do not match');
            }
            $details['password']=$this->secret('Password');
            $details['confirm_password']=$this->secret('confirm password');
        }
        $details['password']=Hash::make($details['password']);
        $details['confirm_password']=Hash::make($details['confirm_password']);
        return $details;
    }



    private function display(User $admin) : void 
    {
        $headers=['Name', 'Email', 'Super admin'];
        $fields=[
            'Name'=>$admin->name,
            'email'=>$admin->email,
            'admin'=>$admin->isSuperAdmin()

        ];
        $this->info('Super admin created');
        $this->table($headers, [$fields]);
    }

    private function isValidPassword(string $password, string $confirmPassword) :bool
    {
        return $this->isRequiredLength($password) &&
        $this->isMatch($password, $confirmPassword);
    }

    private function isMatch(string $password, string $confirmPassword) : bool {
        return $password === $confirmPassword;
    }
    private function isRequiredLength(string $password) : bool {
        return strlen($password) >6;
    }

}
