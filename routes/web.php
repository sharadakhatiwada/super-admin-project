<?php

use Illuminate\Support\Facades\Route;
use App\Models\Match;
use App\Models\SliderModel;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // $sliders= \File::allFiles(public_path('images'));
    $sliders=SliderModel::all();
    $matches = Match::all();
    return view('welcome', compact('sliders','matches'));
    
 
});


Auth::routes();


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('superAdmin','App\Http\Controllers\superAdminController');
Route::resource('match','App\Http\Controllers\MatchController');
Route::resource('slider','App\Http\Controllers\SliderController');


